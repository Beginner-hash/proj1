import React from "react"
import Thumbnail from "./Thumbnail";
 
function Projects(props) {
  return (
    <div>
      <h1>Projects</h1>
      <Thumbnail
        link="https://www.youtube.com/"
        image="https://img.phonandroid.com/2021/03/youtube-logo-e1616502332866.jpg"
        title="Youtube"
        category="Chaine YT"
      />
    </div>
  )
}
 
export default Projects;